# FHWA Visual State Statistical Abstract PDF Builder

# Overview

A Python project to automate the creation of beautiful PDF state statistical abstracts from the FHWA Statistical Series.

The Visual State Statistical Abstracts PDF Generator is a utility that automatically generates three-page visual summaries of the FHWA State Statistical Abstracts. The utility functions by combining built in State Statistical Abstract data from 2010 – 2015 with user-supplied subsequent versions of the Excel version State Statistical Abstracts file to produce the visual summaries.

The State Statistical Abstracts is an information product from the FHWA Office of Highway Policy Information. The abstract tables can be downloaded from [https://www.fhwa.dot.gov/policyinformation/](https://www.fhwa.dot.gov/policyinformation/)

## Abstracts Spreadsheet File

The utility relies on updated abstract data being provided in a consistent format. Each year, the Statistical Abstracts are created from source tables and populated into a Microsoft Excel spreadsheet. To ensure future consistency, this utility includes a protected version of the Excel file which will prevent inadvertent changes to the row/column layout of the source file spreadsheet(s). It is recommended that each year’s update be created from the template Excel file.

# Instructions for Use
The PDF Builder is a web-based application. 

## Building PDFs for 2015
To build PDFs for the year 2015, simply navigate to the utlity home page [/](/) and click the [Process] button. The utility has built-in data for 2010 - 2015 that it will use to generate the visual abstracts. Go grab a cup of coffee. When finished, download the .zip file containing the abstracts, or click on the links to view the individual files.

## Building PDFs for 2016
For 2016, you'll first need a completed and updated State Statistical Abstracts Excel workbook for 2016. Due to slight variations in previous years' Excel files, it is recommended to use the provided [template file](/static/state_abstracts_template.xls) as a starting point for the 2016 abstracts. Once you have a complete source file for 2016, upload this file as the "2016 Spreadsheet File", click Process.

## Building PDFs for 2017 Onward
For 2017 (and subsequent years), you'll first need a completed and updated State Statistical Abstracts Excel workbook for 2017. Due to slight variations in previous years' Excel files, it is recommended to use the provided [template file](/static/state_abstracts_template.xls) as a starting point for the 2016 abstracts. 

You will also need a copy of the 2016 State Statistical Abstracts Excel workbook. If you don't have a copy of this handy, don't worry--this file was probably posted to the [Office of Highway Policy Information Website](https://www.fhwa.dot.gov/policyinformation/) last year. Try [this link](https://www.fhwa.dot.gov/policyinformation/statistics/abstracts/2016/).

Once you have a complete source file for 2017 and the 2016 State Statistical Abstracts Excel workbook, upload the 2016 State Statistical Abstracts Excel workbook file as the "2016 Spreadsheet File", click [+ Add Year](#), and upload the 2017 Excel workbook as the 2017 Spreadsheet File.


# Installation / Deployment

## System Requirements

* Windows Server 2016 or newer, or any modern Linux distribution (Ubuntu 18.04 recommended)
* 2 gb free memory
* 2 gb free disk space


## Server Deployment

The Abstract PDF Builder deploys in three simple steps:

1. Install Docker
2. Download a copy of the Docker Container with the PDF Builder utility
3. Run the Docker Container

### Install Docker

Use the instructions as [https://docs.docker.com/manuals/](https://docs.docker.com/manuals/) to install the appropriate edition of Docker for your server operating system. 

Choose and follow the instructions below for your server operating system:

* [Windows Server 2016](https://docs.microsoft.com/en-us/virtualization/windowscontainers/quick-start/quick-start-windows-server#install-docker)
* [Linux](https://docs.docker.com/install/linux/docker-ce/ubuntu/)

### Download the Container

Once you have successfully tested your local Docker installation, grab the latest image of the Abstract PDF Generator with the command:
`docker pull highst/fhwa-ssa`

### Run the Container

Run the container on port 5000 (or a port of your choosing) using the following command:

`docker run --publish 5000:80 --detach --name ssa --restart unless-stopped highst/fhwa-ssa`

Explanation:

  * `docker run ... highst/fhwa-ssa` runs the locally downloaded copy of the Container
  * `--publish 5000:80` publishes the container's port 80 to the local port 5000. To map to a different local port, simply replace the first port with the local port you wish to use. E.g. to access the container using local port 80, use `--publish 80:80`
  * `--detach` causes the container to run in the background. (To see which containers are currently running, use `docker ps`.)
  * `--name ssa` assigns the name "ssa" to the container (useful for controlling the container later)
  * `--restart unless-stopped` causes the container to automatically restart if it crashes, and to load when the operating system initiates the Docker daemon host process

You should now be able to access the PDF Generator by pointing your local browser to [http://localhost:5000](http://localhost:5000)

You may need to configure appropriate firewall rules to make this service accessible to external users.


## Development Deployment

### Build the app

The Docker container is built by executing the source in the script called Dockerfile (which is a Docker build script). To build the container, run the command below:
From within the top level directory:

```bash
docker build -t ssa .
```

### Run the app

Run the app, mapping your machine’s port 5000 to the container’s EXPOSEd port 80 using -p:

```bash
docker run -it --name ssa --rm -p 5000:80 ssa
```

### Tag the image and publish

```bash
docker tag ssa highst/fhwa-ssa:latest
docker push highst/fhwa-ssa:latest
```

### Pull and run the image from the remote repository

From now on, you can use docker run and run your app on any machine with this command:

```bash
sudo docker pull highst/fhwa-ssa
sudo docker stop ssa
sudo docker rm ssa
sudo docker run --detach --name ssa -p 5000:80 highst/fhwa-ssa
```

### Access the app

Open your web browser to URL [http://localhost:5000](http://localhost:5000)

### Troubleshooting

The log files from the running application can be downloaded from [/static/app.log](/static/app/log)

To connect to your Docker container:
`docker exec -it ssa bash`

To see if an existing container is already running:
`docker ps`

To kill an existing container:

```bash
docker stop ssa
docker rm ssa
```

# Local Development

## Environment

**Conda Environment**
```bash
source activate ssa
export FLASK_APP=app.py
export FLASK_DEBUG=1
flask run
```

**Requirements**
* Python >= 3.7
* matplotlib
* pandas
* weasyprint
(For full requirements, see requirements.txt)

## Troubleshooting

To connect to the running container:

`docker exec -it ssa bash`


## Testing

Test cases are coded into a file called test_SAMPLE.py

```bash
pip install pytest
py.test
```

To run the Flask app at [http://localhost:5000](http://localhost:5000):

```bash
source activate ssa
export FLASK_APP=app.py
export FLASK_DEBUG=1
export FLASK_ENV=development
flask run

```