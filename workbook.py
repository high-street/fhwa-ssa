import openpyxl
import xlrd
import os

class Workbook(object):
    """ 
    Wrapper abstracting .xls and .xlsx files using xlrd and openpyxl
    """
    
    def __init__(self, src, sheet_name = False, sheet_index = 0):
        if src[-4:] == 'xlsx':
            self.xlsx = True
            self.wb = openpyxl.load_workbook(filename = src, data_only=True)
            if sheet_name:
                self.ws = self.wb[sheet_name]
            else:
                self.ws = self.wb[self.wb.sheetnames[sheet_index]]
                
        elif src[-3:] == 'xls':
            self.xlsx = False
            self.wb = xlrd.open_workbook(src)
            if sheet_name:
                self.ws = self.wb.sheet_by_name(sheet_name)
            else:
                self.ws = self.wb.sheet_by_index(sheet_index)
            
        else:
            raise Exception("Invalid file extension. Expected 'xls' or 'xlsx'. File: " + src)
    
    def set_sheet(self, sheet):
        """ updates the internal worksheet based on the sheet name or index """
        if type(sheet) == int:
            sheet_name = False
            sheet_index = 0
        if type(sheet) == str:
            sheet_name = sheet
            
        if self.xlsx:
            if sheet_name:
                self.ws = self.wb[sheet_name]
            else:
                self.ws = self.wb[self.wb.sheetnames[sheet_index]]            

        else:
            if sheet_name:
                self.ws = self.wb.sheet_by_name(sheet_name)
            else:
                self.ws = self.wb.sheet_by_index(sheet_index)

    def sheet_names(self):
        if self.xlsx:
            return self.wb.sheetnames
        else:
            return self.wb.sheet_names()
        
    def get_value(self, row, column):
        """ return cell value based on zero indexing scheme based on row, column """
        if self.xlsx:
            return self.ws.cell(row = row + 1, column = column + 1).value
        else:
            return self.ws.cell_value(row, column)