#%matplotlib inline  # for use during development. Comment out for deployment

import os

import pandas as pd
# from pandas.core.index import InvalidIndexError

import matplotlib
matplotlib.use("Agg")

from matplotlib import pyplot as plt
from matplotlib.font_manager import FontProperties
import matplotlib.dates as mdates
from matplotlib.ticker import FuncFormatter, MaxNLocator, FormatStrFormatter
from zipfile import ZipFile

import datetime

import numpy as np
import re
import math

import shutil

import xlrd

from jinja2 import Environment, FileSystemLoader, select_autoescape

import weasyprint
import logging
logger = logging.getLogger('weasyprint')
logger.handlers = []  # Remove the default stderr handler
logger.addHandler(logging.FileHandler('weasyprint.log'))

# define some default plot styling
from matplotlib import rcParams
rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Lucida Grande', 'Tahoma']
rcParams['ytick.labelsize'] = 8
rcParams['text.color'] = '#333333'


LIGHT_BLUE='#9dc3e6'
BLUE='#1e83df'
DARK_BLUE='#003471'
ORANGE='#ed7d31'
LIGHT_ORANGE='#f4b183'
DARK_ORANGE='#c55a11'
GRAY = '#a0a0a0'
DARK_GRAY = '#555555'
LIGHT_GRAY = '#d8d8d8'

CM = [LIGHT_BLUE, LIGHT_ORANGE, GRAY, DARK_BLUE, LIGHT_BLUE, LIGHT_ORANGE, GRAY, DARK_BLUE]
CM_DARK = [DARK_BLUE, DARK_ORANGE, DARK_GRAY, '#001550', DARK_BLUE, DARK_ORANGE, DARK_GRAY, '#001550']

f_systems = ['Local','Major Collector', 'Minor Collector', 'Minor Arterial', 
                  'Other Principal Arterials','Other Freeways & Expressways','Interstate']
f_system_titles = ['Local', 'Major Collector', 'Minor Collector', 'Minor Arterial',
             'Principal Arterials', 'Freeways & Expressways', 'Interstate']


 # function for "pretty" formatting of chart units
def millify(n, pos = None):
    millnames = ['','k',' M',' B',' T'] # _, thousand, million, billion, trillion
    n = float(n)
    millidx = max(0,min(len(millnames)-1, int(math.floor(0 if n == 0 else math.log10(abs(n))/3))))
    num = n / 10**(3 * millidx)
    return '{}{}'.format(round(num, 1), millnames[millidx])
#    if num < 100:
#        return '{}{}'.format(round(num, 1), millnames[millidx])
#    else:
#        return '{}{}'.format(round(num, 0), millnames[millidx])

def template_values(df, state):
    k = v = col = topic = item = None
    lookups = {
        'population': ('Value', 'Population / Land Area', 'Total Population'),
        "population_percent" : ('Share of National', 'Population / Land Area', 'Total Population'),
        "land_area" : ('Value', 'Population / Land Area', 'Total Land Area'),
        "land_area_percent" : ('Share of National', 'Population / Land Area', 'Total Land Area'),
        "vmt" : ('Value', 'Vehicle Miles Traveled', 'Total'),
        "vmt_percent" : ('Share of National', 'Vehicle Miles Traveled', 'Total'),
        "road_length" : ('Value', 'Public Road Length', 'Total'),
        "road_length_percent" : ('Share of National', 'Public Road Length', 'Total'),
        "lane_miles" : ('Value', 'Functional System Lane Length', 'Total'),
        "lane_miles_percent" : ('Share of National', 'Functional System Lane Length', 'Total'),
        "vehicle_registrations": ('Value', 'Vehicle Registrations', 'Grand Total'),
        "vehicle_registrations_percent": ('Share of National', 'Vehicle Registrations', 'Grand Total'),
        "drivers_licenses": ('Value', 'Driver Licenses', 'Total'),
        "drivers_licenses_percent": ('Share of National', 'Driver Licenses', 'Total'),
        "motor_fuel_use": ('Value', 'Motor Fuel Usage', 'Total Fuel'),
        "gasoline": ('Value', 'Motor Fuel Tax Rates', 'Gasoline'),
        "diesel": ('Value', 'Motor Fuel Tax Rates', 'Diesel'),
        "highway_funding_revenue": ('Value', 'Revenues Received by State for Highways', 'Total'),
        "highway_funding_expenditures": ('Value', 'Disbursements Used by States for Highways', 'Total')
    }
    
    try:
        year = df['Year'].max()

        sdf = df.loc[(df['State'] == state) & (df['Year'] == year)]
        sdf.set_index(['Topic', 'Item'], inplace=True)

        state_year = df['Year'].loc[df['State'] == state].max()
        
        if year != state_year:
            msg = ('Error populating template values: data source year %s ' + 
                   'does not match most recent processing year %s. Please ' + 
                   'check source file.') % (str(state_year), str(year))
            raise Exception(msg)

    
        templateVars = { 
            "state": state.title(),
            "state_stub" : state.lower().replace(' ', '_'),
            "year" : year,
        }
        
        for k, v in lookups.items():
            col, topic, item = v
            templateVars[k] = sdf[col].loc[topic, item]
        
    except (IndexError, KeyError, Exception) as err:
        err.custom='Index Error populating template values'
        
        if k and col and topic and item:
            err.custom += ' for %s based on %s - %s %s' % (k, topic, item, col)
            
        err.state = state
        err.year = year

        raise

    except Exception as err:
        err.custom='Non-Index Error populating template values'
        
        if k and col and topic and item:
            err.custom += ' for %s based on %s - %s %s' % (k, topic, item, col)
            
        err.state = state
        err.year = year

        raise
        
    return templateVars


# share of national plot
# state_name='Alabama'
# state_template_values = template_values(df, state_name)
# if True:

def p1_pct_ntnl_plot(state_template_values):
    plt.clf()
    plt.rcdefaults()
    fig, ax = plt.subplots(figsize=(2.75, 2.), dpi=96)

    facts = ('Population', 'Land Area', 'VMT', 'Public Road Length', 'Public Lane Miles')
    y_pos = np.arange(len(facts))
    val_vars = ['population_percent', 'land_area_percent', 'vmt_percent', 'road_length_percent', 'lane_miles_percent']
    values = [state_template_values[k] for k in val_vars] #careful, here--order not guaranteed
    values_max = pd.Series(values).max()
    ax.barh( y_pos, values, 0.6, color=LIGHT_ORANGE, edgecolor=DARK_ORANGE)

    ax.set_title('Share of National')
    ax.tick_params(labelsize=8)

    # y axis
    ax.set_yticks(y_pos)
    ax.set_yticklabels(facts)
    ax.yaxis.set_ticks_position('none') 
    ax.invert_yaxis()
    locator = MaxNLocator(nbins=3)
    ax.xaxis.set_major_locator(locator)
        
    # x ticks
    ax.xaxis.set_major_formatter(FuncFormatter(lambda x, _: '{:.1%}'.format(x)))
    ax.xaxis.set_ticks_position('none') 
    ax.xaxis.grid(True) 
    ax.set_axisbelow(True)
    ax.locator_params(axis='x', nbins=4)
    
    # data value labels
    for i, v in enumerate(values):
        if v > values_max * .22:
            hloc = v - .03*values_max
            align = 'right'
        else:
            hloc = v + .03 * values_max
            align = 'left'
        
        ax.text(hloc, i, '{:.1%}'.format(v), color=DARK_GRAY, va='center', ha=align)

    # border
    [spine.set_visible(False) for spine in ax.spines.values()]
    
	#  plt.show() # for debugging purposes
    fig_path = os.path.join('static', 'images', 'plots', 
                            state_template_values['state_stub'], 'percent_of_national.png')
    plt.savefig(fig_path, dpi=300, bbox_inches='tight', pad_inches=0) # 
    plt.close()



# state_name='Alabama'
# state_template_values = template_values(df, state_name)
# if True:

def p1_line_charts(df, state_template_values):
    state = state_template_values['state']
    if state == 'Dist. Of Col.':
        state = 'Dist. of Col.'
    plt.clf()
    plt.rc('lines', linewidth=2)
    year = df['Year'].max()
    yrs = np.arange(year - 4, year + 1, 1)

    charts = [
        ('Population / Land Area', 'Total Population', 'Population'),
        ('Vehicle Miles Traveled', 'Total', 'VMT'),
        ('Driver Licenses', 'Total', 'Driver Licenses'),
        ('Vehicle Registrations', 'Grand Total', 'Vehicle Registrations'),
        ('Motor Fuel Usage','Total Fuel', 'Motor Fuel Use (Gallons)')
    ]

    fig, axes = plt.subplots(
        nrows=5, ncols=1, 
        sharex=True, sharey=False, 
        figsize=(4, 9), dpi=96
    )

    fig.subplots_adjust(hspace=.32)

    formatter = FuncFormatter(millify)

    for i, ax in enumerate(axes):
        try:
            
            df1 = df.loc[
                (df['State'] == state) & 
                (df['Year'] > year - 5) & 
                (df['Topic'] == charts[i][0]) & 
                (df['Item'] == charts[i][1])
            ].copy()

            df1 = pd.concat([df1.set_index('Year'), pd.DataFrame(None, index=yrs)], axis=1)
        except InvalidIndexError as err:
            err.state = state
            err.year = year
            err.custom = 'Page 1 Line Charts'
            print(df1)
            raise

        except Exception as err:
            err.state = state
            err.year = year
            err.custom = 'Page 1 Line Charts'
            print(df1)
            raise
            
        if len(df1) < 5:
            print('Fewer than five years for %s %s' % (charts[i][0], charts[i][1]))
            print(df1)
            continue
        
        values = df1['Value']

        # if VMT, multiply by 1m
        if charts[i][0] == 'Vehicle Miles Traveled':
            values = values.astype(np.int64).multiply(np.int64(1000000))
            
        if charts[i][0] == 'Motor Fuel Usage':
            values = values.astype(np.int64).multiply(np.int64(1000))
        
        for value in values:
            if value < 0:
                err = "p1_line_charts error: invalid value. State: %s, Year: %s, Value: %s" % (state, year, value)
                raise ValueError(err)

        y = 1.25 if i == 0 else 1.0
        ax.set_title(charts[i][2], y=y)

        colors = ['#689AD0', '#E08244', '#A6A6A6']
        try:
            ax.plot(yrs, values, linestyle='-', marker='o', color=colors[i % 3])
        except ValueError:
            print('Error preparing plot.')
            print(df1)
            print(yrs, values)
            raise
                               
        # Y axis
        locator = MaxNLocator(nbins=4)
        ax.yaxis.set_major_locator(locator)

        ax.yaxis.set_major_formatter(formatter)
        ax.yaxis.grid(True, zorder=0)
        ax.yaxis.set_ticks_position('none') 
        #print(df1)
        ax.set_ylim(values.min()*.85, values.max()*1.15)

        # X axis
        ax.xaxis.set_ticks(yrs)
        ax.tick_params(labelbottom='on' if i == 4 else 'off', labeltop='on' if i == 0 else 'off',)
        ax.xaxis.set_ticks_position('none') 

        # annotate the most recent value
        ax.annotate(millify(values.iloc[-1], None),
            xy=(.92, values.iloc[-1]), 
            xytext=(-2, 6), 
            xycoords=('axes fraction', 'data'), 
            textcoords='offset points'
        )

        ax.autoscale_view()
    
        # border
        [spine.set_visible(False) for spine in ax.spines.values()]
        
	# plt.show()
    fig_path = os.path.join('static', 'images', 'plots', state_template_values['state_stub'], 'p1_charts.png')
    plt.savefig(fig_path, dpi=300, bbox_inches='tight', pad_inches=0)
    plt.close()


# Vehicle Registrations Chart
from matplotlib.offsetbox import (OffsetImage,AnnotationBbox)
from matplotlib.cbook import get_sample_data
import matplotlib.image as mpimg

# state_name='Alabama'
# state_template_values = template_values(df, state_name)
# if True:
    
def vehicle_registrations_chart(df, state_template_values):
    state_name = state_template_values['state']
    if state_name == 'Dist. Of Col.':
        state_name = 'Dist. of Col.'

    year = df['Year'].max()
    plt.clf()

    fig, ax = plt.subplots(figsize=(5.5, 1.15), dpi=96)
    plt.subplots_adjust(left=0, right=1)

    try:
        df1 = df.loc[
                (df['State'] == state_name) & 
                (df['Year'] == year) & 
                (df['Topic'] == 'Vehicle Registrations')
            ].copy()

#        items = ['Trucks', 'Autos', 'Motorcycles', 'Buses']
        items = ['Trucks', 'Automobiles', 'Motorcycles', 'Buses']
        df1 = df1.set_index('Item')
        total = df1.loc['Grand Total', 'Value']
        values = [df1.loc[k, 'Value'] for k in items]   
    except Exception as err:
        err.state = state_name
        err.year = year
        err.custom = 'Vehicle Registrations Chart'
        raise        
        
    width_pct = total / 100
    min_width = width_pct * 15
    
    cumsum = 0
    ticks = []
    
    for i, value in enumerate(values):
        if value==np.nan or value==0:
            continue
        if value < width_pct:
            value = width_pct
            
        if value < min_width:
            cumsum += .5 * (min_width - value)
        ax.barh(.5, value, height=.27, left=cumsum, align='center', color=CM[i], edgecolor=CM_DARK[i] )
        
        ticks.append(cumsum + .5*value)
        
        cumsum += value
        
        if i == 0:
            cumsum += 2.5 * width_pct  # add some padding between cars and trucks
            
        if value < min_width:
            cumsum += .5 * (min_width - value)
        
    ax.set_ylim(0, 1)

    ax.get_yaxis().set_visible(False)
    ax.get_xaxis().set_visible(False)

    images = [os.getcwd() + '/static/images/trucks.png',
              os.getcwd() + '/static/images/autos.png',
              os.getcwd() + '/static/images/motorcycles.png',
              os.getcwd() + '/static/images/buses.png']
    
    # data value labels
    for i, value in enumerate(values):
        if value==np.nan or value==0:
            continue
        ax.text(ticks[i], .7, '{:,.0f}'.format(value), ha='center', fontsize=10)
        ax.text(ticks[i], .85, items[i], ha='center', family='DejaVu Sans', fontsize=8)

        fn = get_sample_data(images[i], asfileobj=False)
        arr_img = plt.imread(fn, format='png')
        im = OffsetImage(arr_img, zoom=1, dpi_cor=False)
        xy = (ticks[i],.18)
        ab = AnnotationBbox(im, xy, xycoords='data', frameon=False)
        ax.add_artist(ab)

    # border
    [spine.set_visible(False) for spine in ax.spines.values()]

	# plt.show() # for debugging purposes
    fig_path = os.path.join('static', 'images', 'plots', 
                            state_template_values['state_stub'], 'vehicle_registrations.png')
    plt.savefig(fig_path, dpi=300, bbox_inches="tight", pad_inches=0)
    plt.close()



# Public Road Length Chart

# state_name='Alabama'
# state_template_values = template_values(df, state_name)
# if True:
def road_length_chart(df, state_template_values):
    state_name = state_template_values['state']
    if state_name == 'Dist. Of Col.':
        state_name = 'Dist. of Col.'
    year = df['Year'].max()
    plt.clf()

    fig, axes = plt.subplots(
        nrows=1, 
        ncols=2, 
        sharex=False, 
        sharey=True, 
        figsize=(7.5, 2.65), 
        dpi=96
    )
    plt.subplots_adjust(wspace=.18)

    # Public Road Length Plot
	# topics = ['Public Road Length by Functional System', 'Functional System Lane Length']
    topics = ['Public Road Length', 'Functional System Lane Length']
    titles = ['Public Road Length', 'Public Road Lane Miles']
    
    for i, ax in enumerate(axes):
	# ax.set_title(titles[i])
	# ttl = ax.title
	# ttl.set_position([0.5, 1.125])
        try:
            df1 = df.loc[
                    (df['State'] == state_name) & 
                    (df['Year'] == year) & 
                    (df['Topic'] == topics[i])
                ].copy()

            df1 = df1.set_index('Item')
            #display(df1)

            values = [df1.loc[k, 'Value'] for k in f_systems] #careful, here--order not guaranteed
            pct_state = [df1.loc[k, 'Distribution'] for k in f_systems] # percent of state
            pct_national = [df1.loc[k, 'Share of National'] for k in f_systems]

            values_max = pd.Series(values).max() 
        except Exception as err:
            err.state = state_name
            err.year = year
            err.custom = 'Public Road Length Chart'
            raise    
        
        y_pos = np.arange(len(values))
        ax.barh(y_pos,  values, 0.6,
                    align='center', color='#9dc3e6', edgecolor='#003471', zorder=5)
        
        ax.yaxis.set_ticks_position('none') 
        ax.set_xlim(0, values_max*1.25)
        
        if i == 0:
            # y axis
            y_axis_font = FontProperties()
            y_axis_font.set_size('small')
            ax.set_yticks(y_pos)
            ax.set_yticklabels(f_system_titles, fontproperties=y_axis_font)
            ax.invert_yaxis()
            
        # border
        [spine.set_visible(False) for spine in ax.spines.values()]

        # x ticks
        ax.tick_params(labelbottom='off')
        ax.xaxis.set_ticks_position('none')

        # data value labels
        for i, v in enumerate(values):
            h_loc = v + 0.02*values_max if v < (values_max*.33) else v - .01*values_max

            opts = {
                'color':'#003471', 
                'fontweight':'normal',
                'fontsize': 10,
                'zorder':10
            }

            if v > (values_max*.33): 
                opts['horizontalalignment'] = 'right'

            ax.text(0.0, 1.025, '(in miles)',
                transform=ax.transAxes,
                color='gray', fontsize=9, zorder=15)

            ax.text(h_loc, i + .12, '{:,.0f}'.format(v), **opts)

            ax.text(.9, 1.07, 'Percent of State',
                    color='black', fontsize='small',
                    transform=ax.transAxes, horizontalalignment='center')

            ax.text(1.12*values_max, i, '{:.1%}'.format(pct_state[i]),
                    color='black', fontweight='medium', fontsize='small',
                    horizontalalignment='center')

            ax.text(.9, 1.017, 'Percent of National', 
                    color='#9DC3E6', fontstyle='italic', fontsize='small',
                    transform=ax.transAxes, horizontalalignment='center')

            ax.text(1.12*values_max, i + 0.33, '{:.1%}'.format(pct_national[i]), 
                    color='#9DC3E6', fontweight='medium', 
                    fontstyle='italic', fontsize='small',
                    horizontalalignment='center')

	#  plt.show() # for debugging purposes
    fig_path = os.path.join('static', 'images', 'plots', state_template_values['state_stub'], 'road_length.png')
    plt.savefig(fig_path, dpi=300, bbox_inches="tight")
    plt.close()


# Public Road Length Chart

# state_name='Alabama'
# state_template_values = template_values(df, state_name)

# if True:
def share_of_road_miles_chart(df, state_template_values):
    year = df['Year'].max()
    state_name = state_template_values['state']
    if state_name == 'Dist. Of Col.':
        state_name = 'Dist. of Col.'
    plt.clf()

    fig, axes = plt.subplots(
        nrows=1, 
        ncols=2, 
        sharex=False, 
        sharey=True, 
        figsize=(3.75, 2.5), 
        dpi=96
    )
    plt.subplots_adjust(wspace=0, top=.77, left=.42, right=1, bottom=0)

#--->    topics = ['Public Road Length by Functional System', 'Vehicle Miles Traveled by Functional System']
    topics = ['Public Road Length', 'Vehicle Miles Traveled']
    
    colors = [LIGHT_BLUE, ORANGE]
    edge_colors = [DARK_BLUE, DARK_ORANGE]
    
    for i, ax in enumerate(axes):
        try:
            df1 = df.loc[
                    (df['State'] == state_name) & 
                    (df['Year'] == year) & 
                    (df['Topic'] == topics[i])
                ].copy()

            df1 = df1.set_index('Item')
            values = [df1.loc[k, 'Distribution'] for k in list(f_systems)] # percent of state
        
        except Exception as err:
#            err.state = state
            err.state = state_name
            err.year = year
            err.custom = 'Length / VMT by Functional System'
            raise
            
        ax.set_xlim(0, 1)
        ax.set_ylim(0, 1)
        
        inc = 1/len(values)
        x_pos = np.empty(len(values)); x_pos.fill(.55)
        y_pos = np.linspace(1 - inc, 0.075, endpoint=True, num=len(values))
        area = [8 * (25*v)**1.5 for v in values] # 0 to 20 point radii
        
        # bubble plot
        ax.scatter(x_pos, y_pos, s=area, c=colors[i], linewidths=0.5, edgecolors=edge_colors[i])
        
        # y axis
        y_axis_font = FontProperties()
        y_axis_font.set_size('small')
        ax.yaxis.set_ticks_position('none') 
        ax.set_yticks(y_pos)
        ax.set_yticklabels(f_system_titles, fontproperties=y_axis_font)
            
        # border
        for s, spine in enumerate(ax.spines.values()):
            if i==0 and s==0:
                spine.set_color("#CCCCCC")
            else:
                spine.set_visible(False)

        # x ticks
        ax.tick_params(labelbottom='off'); ax.xaxis.set_ticks_position('none')

        ax.text(0.5, 1.2, ['Road Length', 'VMT'][i], horizontalalignment='center', fontsize=11)
        ax.text(0.5, 1.115, ['(miles)', '(millions)'][i], horizontalalignment='center',  fontsize=10)
        
        # total miles / vmt value
        ax.text(0.5, 1.0, '{:,.0f}'.format(state_template_values[['road_length', 'vmt'][i]]),
            horizontalalignment='center',
            fontsize=12, fontweight='bold', zorder=15)
        
        # data value labels
        for j, v in enumerate(values):
            ax.text(.5 - .15*v, y_pos[j]-.01, '{:.1%}'.format(v), 
                    fontsize=9, verticalalignment='center', horizontalalignment='right')

    #plt.show() # for debugging purposes
    fig_path = os.path.join('static', 'images', 'plots', 
                            state_template_values['state_stub'], 'share_of_road_miles.png')
    #plt.savefig(fig_path, dpi=300, bbox_inches="tight")
    plt.savefig(fig_path, dpi=300)
    plt.close()

# Urban / Rural Split Chart

# state_name='Alabama'
# state_template_values = template_values(df, state_name)
# if True:
    
def road_length_vmt_rural_urban_chart(df, state_template_values):
    year = df['Year'].max()
    state_name = state_template_values['state']
    if state_name == 'Dist. Of Col.':
        state_name = 'Dist. of Col.'

    try: 
        df1 = df.loc[(df['State'] == state_name) & (df['Year'] == year)].copy()
        df1.set_index(['Topic', 'Item'], inplace=True)

        length_pct_rural = df1['Distribution'].loc['Public Road Length', 'Rural']
        length_pct_urban = df1['Distribution'].loc['Public Road Length', 'Urban']
        vmt_pct_rural = df1['Distribution'].loc['Vehicle Miles Traveled', 'Rural']
        vmt_pct_urban = df1['Distribution'].loc['Vehicle Miles Traveled', 'Urban']

    except Exception as err:
#        err.state = state
        err.state = state_name
        err.year = year
        err.custom = 'Urban / Rural Split Chart'
        raise
        
    # road length chart
    plt.clf()    
    fig, ax = plt.subplots(figsize=(2.75, .8), dpi=96)
    
    ax.set_xlim(0, 1); ax.set_ylim(0, 1)
    ax.get_yaxis().set_visible(False); ax.get_xaxis().set_visible(False)
    [spine.set_visible(False) for spine in ax.spines.values()]
        
    p1 = plt.barh(.7, length_pct_rural, .33, color=LIGHT_BLUE, edgecolor=DARK_BLUE, linewidth=.5)
    p2 = plt.barh(.7, length_pct_urban, .33, left=length_pct_rural, color=DARK_BLUE, edgecolor=DARK_BLUE, linewidth=.5)

    # labels
    if length_pct_rural > 0:
        ax.text(.03, .7, '{:.0%}'.format(length_pct_rural), color='#202020', size=9, va='center')
    ax.text(.97, .7, '{:.0%}'.format(length_pct_urban), color='white', size=9, va='center', ha='right')

    if length_pct_rural > 0:
        ax.text(.03, .3, 'Rural', color=LIGHT_BLUE, size=9)
    ax.text(.97, .3, 'Urban', color=DARK_BLUE, size=9, ha='right') 
    
	#     plt.show() # for debugging purposes
    fig_path = os.path.join('static', 'images', 'plots', 
                            state_template_values['state_stub'], 'road_length_rural_urban.png')
    plt.savefig(fig_path, dpi=300, bbox_inches='tight', pad_inches=0)
    plt.close()
    
    # #########
    # VMT chart
    # #########
    plt.clf()    
    fig, ax = plt.subplots(figsize=(2.75, .8), dpi=96)
    
    ax.set_xlim(0, 1); ax.set_ylim(0, 1)
    ax.get_yaxis().set_visible(False); ax.get_xaxis().set_visible(False)
    [spine.set_visible(False) for spine in ax.spines.values()]       

    p3 = plt.barh(.7, vmt_pct_rural, .33, color=LIGHT_ORANGE, edgecolor=DARK_ORANGE, linewidth=.5)
    p4 = plt.barh(.7, vmt_pct_urban, .33, left=vmt_pct_rural, color=DARK_ORANGE, edgecolor=DARK_ORANGE, linewidth=.5)

    # labels
    ax.text(.03, .7, '{:.0%}'.format(vmt_pct_rural), color='#202020', size=9, va='center')
    ax.text(.97, .7, '{:.0%}'.format(vmt_pct_urban), color='white', size=9, va='center', ha='right') 

    if vmt_pct_rural > 0:
        ax.text(.03, .3, 'Rural', color=DARK_ORANGE, size=9)
    ax.text(.97, .3, 'Urban', color=DARK_ORANGE, size=9, ha='right') 
    
	#     plt.show() # for debugging purposes
    fig_path = os.path.join('static', 'images', 'plots', 
                            state_template_values['state_stub'], 'vmt_rural_urban.png')
    plt.savefig(fig_path, dpi=300, bbox_inches='tight', pad_inches=0)
    plt.close()    


# Vehicle Miles Traveled Chart

# state_name='Alabama'
# state_template_values = template_values(df, state_name)
# if True:
    
def vehicle_miles_traveled_chart(df, state_template_values):
    state_name = state_template_values['state']
    if state_name == 'Dist. Of Col.':
        state_name = 'Dist. of Col.'
    year = df['Year'].max()
    plt.clf()

    fig, ax = plt.subplots(figsize=(3.85, 2.6), dpi=96)
    plt.subplots_adjust(wspace=0, top=.77, left=.42, right=1, bottom=0.01)
    
    try:
        df1 = df.loc[
                (df['State'] == state_name) & 
                (df['Year'] == year) & 
#                (df['Topic'] == 'Vehicle Miles Traveled by Functional System')
                (df['Topic'] == 'Vehicle Miles Traveled')
            ].copy()

        df1 = df1.set_index('Item')
        values = [df1.loc[k, 'Value'] for k in f_systems] 
        pct_state = [df1.loc[k, 'Distribution'] for k in f_systems] # percent of state
        pct_national = [df1.loc[k, 'Share of National'] for k in f_systems]

    except Exception as err:
#        err.state = state
        err.state = state_name
        err.year = year
        err.custom = 'VMT Chart'
        raise
        
    values_max = pd.Series(values).max() 

    y_pos = np.arange(len(values))
    
    ax.barh(y_pos, values, 0.65, align='center', color=LIGHT_ORANGE, edgecolor=DARK_ORANGE )
    
    ax.set_xlim(0, values_max*1.25)

    # y axis
    ax.yaxis.set_ticks_position('none') 
    y_axis_font = FontProperties()
    y_axis_font.set_size('small')
    ax.set_yticks(y_pos)
    ax.set_yticklabels(f_system_titles, fontproperties=y_axis_font)
    ax.invert_yaxis()

    # border
    [spine.set_visible(False) for spine in ax.spines.values()]

    # x ticks
    ax.tick_params(labelbottom='off')
    ax.xaxis.set_ticks_position('none')

    # data value labels
    for i, v in enumerate(values):
        ax.text(0.0, 1.0, '(in miles)', transform=ax.transAxes,
            color='gray', fontsize=9, zorder=15)
        
        h_loc = v + 0.02*values_max if v < (values_max*.25) else v - .01*values_max
        ax.text(h_loc, i, millify(v*1000000), fontsize=9, va='center', ha='right' if  v > (values_max*.25) else 'left')

        ax.text(.9, 1.05, 'Percent of State', color='black', fontsize='small',
                transform=ax.transAxes, horizontalalignment='center')

        ax.text(1.12*values_max, i - 0.02, '{:.1%}'.format(pct_state[i]),
                color='black', fontweight='medium', fontsize='small',
                horizontalalignment='center')

        ax.text(.9, .99, 'Percent of National',
                color=DARK_ORANGE, fontstyle='italic', fontsize='small',
                transform=ax.transAxes, horizontalalignment='center')

        ax.text(1.12*values_max, i + 0.35, '{:.1%}'.format(pct_national[i]), 
                color=DARK_ORANGE, fontweight='medium', 
                fontstyle='italic', fontsize='small',
                horizontalalignment='center')

#     plt.show() # for debugging purposes
    fig_path = os.path.join('static', 'images', 'plots', 
                            state_template_values['state_stub'], 'vehicle_miles_traveled.png')
    plt.savefig(fig_path, dpi=300, bbox_inches="tight", pad_inches=0)
    plt.close()


# New Lane Miles Chart

# state_name='Alabama'
# state_template_values = template_values(df, state_name)
# if True:
    
def new_lane_miles_chart(df, state_template_values):
    state_name = state_template_values['state']
    if state_name == 'Dist. Of Col.':
        state_name = 'Dist. of Col.'
    year = df['Year'].max()
    plt.clf()

    fig, ax = plt.subplots(figsize=(3.15, 2), dpi=96)
   
    try:
        df1 = df.loc[
                (df['State'] == state_name) & 
                (df['Year'] > year - 6) & 
                (df['Topic'] == 'Functional System Lane Length') &
                (df['Item'] == 'Total')
            ].copy()
        df1.sort_values('Year', inplace=True, ascending=True)
        years = df1['Year'].tolist()[1:]
        values = df1['Value'].tolist()
        values = [values[i] - values[i-1] for i in range(1, len(values))]

    except Exception as err:
#        err.state = state
        err.state = state_name
        err.year = year
        err.custom = 'New Land Miles Chart'
        raise
        
    ax.bar(years, values, 0.65, align='center', color=LIGHT_BLUE, edgecolor=DARK_BLUE, zorder=5 )
    
    ax.yaxis.grid(True) 
    ax.yaxis.set_ticks_position('none')
    plt.yticks(fontsize=9)
    plt.xticks(years, fontsize=8)
    ax.xaxis.set_ticks_position('none')
    # format y axis labels X,XXX
    ax.get_yaxis().set_major_formatter(FuncFormatter(lambda x, pos: '{:,.0f} mi'.format(x) ))
    
    # border
    [spine.set_visible(False) for spine in ax.spines.values()]

    # data value labels
    for i, v in enumerate(values):
        v_max = ax.get_ylim()[1]
        ax.text(years[i], v + v_max*.1, '{:,.0f}'.format(v), 
                fontsize=9, ha='center', va='center', zorder=10)

#     plt.show() # for debugging purposes
    fig_path = os.path.join('static', 'images', 'plots', 
                            state_template_values['state_stub'], 'new_lane_miles.png')
    plt.savefig(fig_path, dpi=300, bbox_inches="tight", pad_inches=0)
    plt.close()

# Vehicle Crash Fatalities Chart
from pandas.core.indexing import IndexingError
import matplotlib.patches as mpatches  

# state_name='Connecticut'
# state_template_values = template_values(df, state_name)
# if True:

def vehicle_crash_fatalities_chart(df, state_template_values):
    state_name = state_template_values['state']
    if state_name == 'Dist. Of Col.':
        state_name = 'Dist. of Col.'

    year = df['Year'].max()
    plt.clf()

    fig, ax1 = plt.subplots(figsize=(6, 2), dpi=96)
    ax2 = ax1.twinx()
    
    try:
        df1 = df.loc[
                (df['State'] == state_name) & 
                (df['Year'] > year - 5)
            ].copy()

        df1 = df1.set_index(['Topic', 'Item', 'Year'])
        df1 = df1.sort_index(level=['Topic', 'Item', 'Year'])

        years = list(range(year - 4, year + 1))

        rural = df1['Value'].loc['Fatally Injured in Vehicle Crashes', 'Rural']
        urban = df1['Value'].loc['Fatally Injured in Vehicle Crashes', 'Urban']
        total = df1['Value'].loc['Fatally Injured in Vehicle Crashes', 'Total']

        vmt = df1['Value'].loc['Vehicle Miles Traveled', 'Total'].tolist()

    except Exception as err:
#        err.state = state
        err.state = state_name
        err.year = year
        err.custom = 'Vehicle Crash Fatalities Chart'
        raise
        
    unknown = []
    for year in years:
        try:
            unknown.append(df1['Value'].loc['Fatally Injured in Vehicle Crashes', 'Unknown', year])
        except IndexingError:
            unknown.append(0)    
    

    y_pct = np.array(total).max() / 100 
    
    vmt_handle, = ax2.plot(years, vmt, label='VMT', linestyle='-', marker='o', 
                           color='#606060', alpha=.7)
    
    urban_bottom = [a + b for a, b in zip(rural, unknown)]

    ax1.bar(years, unknown, 0.42, label='Unknown', align='center', color=GRAY, edgecolor=DARK_GRAY) #
    ax1.bar(years, rural, 0.42, label='Rural', bottom=unknown, align='center', color=LIGHT_ORANGE, edgecolor=DARK_ORANGE) # 
    ax1.bar(years, urban, 0.42, label='Urban', bottom=urban_bottom, align='center', color=LIGHT_BLUE, edgecolor=DARK_BLUE)# 

    ax1.yaxis.grid(True) 
    ax1.set_axisbelow(True)
    ax1.yaxis.set_ticks_position('none')
    ax2.yaxis.set_ticks_position('none')
    
    ax1.get_xaxis().set_visible(False)
    ax2.set_ylim([np.array(vmt).min() * .85, np.array(vmt).max() * 1.2])
    
    plt.yticks(fontsize=9)
    ax1.xaxis.set_ticks_position('none')
    
    ax1.get_yaxis().set_major_formatter(FuncFormatter(lambda x, pos: '{:,.0f}'.format(x) ))
    ax2.get_yaxis().set_visible(False)
    
    ax2.text(years[0], vmt[0]*1.03, 'VMT', color='#222222', ha='center', size=9)
    # border
    [spine.set_visible(False) for i, spine in enumerate(list(ax1.spines.values()) + list(ax2.spines.values())) if i not in [2, 6]]
    
    # data value labels
    for i, year in enumerate(years):
        ax1.text(year, -3.5*y_pct, year, va='top', ha='center')
        ax2.text(year, vmt[i]*.96, millify(vmt[i]*1e6), va='top', 
                 color='#222222', ha='center', size=8)
        
    rural_patch = mpatches.Patch(color=LIGHT_ORANGE, label='Rural')
    urban_patch = mpatches.Patch(color=LIGHT_BLUE, label='Urban')
    unknown_patch = mpatches.Patch(color=GRAY, label='Unknown')
    
    plt.legend(handles=[vmt_handle, unknown_patch, urban_patch, rural_patch], frameon=False,
               loc='lower center', fontsize=9, ncol=4, bbox_to_anchor=(0.5, -.35))      
        
	# plt.show() # for debugging purposes
    fig_path = os.path.join('static', 'images', 'plots', 
                            state_template_values['state_stub'], 'vehicle_crash_fatalities.png')
    plt.savefig(fig_path, dpi=300, bbox_inches="tight", pad_inches=0)
    plt.close()

# state_name='Alabama'
# state_template_values = template_values(df, state_name)
# if True:
    
def driver_licenses_chart(df, state_template_values):
    state_name = state_template_values['state']
    if state_name == 'Dist. Of Col.':
        state_name = 'Dist. of Col.'
    year = df['Year'].max()
    plt.clf()

    fig, ax = plt.subplots(figsize=(5, 1.5), dpi=96)
    plt.subplots_adjust(left=0, right=1)
    
    try:
        df1 = df.loc[
                (df['State'] == state_name) & 
                (df['Year'] > year - 5) & 
                (df['Topic'] == 'Driver Licenses')
            ].copy()


        df1 = df1.set_index('Item')
        males = np.array(df1.loc['Male', 'Value'])
        females = np.array(df1.loc['Female', 'Value'])
        years = np.arange(year - 4, year + 1, 1)

    except Exception as err:
#        err.state = state
        err.state = state_name
        err.year = year
        err.custom = 'Drivers Licenses Chart'
        raise
        
    formatter = FuncFormatter(millify)

    ax.plot(years, females, linestyle='-', marker='o', color=ORANGE, markerfacecolor='white')

    ax.plot(years, males, linestyle='-', marker='o', color=BLUE, markerfacecolor='white')

    # labels
    if females[0] > males [0]:
        ax.text(years[0], females[0]*1.04, 'Females',  ha='center', size=9, color=ORANGE)
        ax.text(years[0], males[0]*.95, 'Males',  ha='center', size=9, va='top', color=BLUE)
    else:
        ax.text(years[0], females[0]*.95, 'Females',  ha='center', size=9, va='top', color=ORANGE)
        ax.text(years[0], males[0]*1.04, 'Males',  ha='center', size=9, color=BLUE)
       
    if females[-1] > males[-1]:
        ax.text(years[-1], females[-1]*1.04, '{:,.0f}'.format(females[-1]),  ha='center', size=9, color=ORANGE)
        ax.text(years[-1], males[-1]*.95, '{:,.0f}'.format(males[-1]),  ha='center', va='top', size=9, color=BLUE)
    else:
        ax.text(years[-1], females[-1]*.95, '{:,.0f}'.format(females[-1]), va='top', ha='center', size=9, color=ORANGE)
        ax.text(years[-1], males[-1]*1.04, '{:,.0f}'.format(males[-1]),  ha='center', size=9, color=BLUE)
        
    # Y axis
    locator = MaxNLocator(nbins=4)
    ax.yaxis.set_major_locator(locator)
    ax.xaxis.set_ticks_position('none') 
    
    ax.yaxis.set_major_formatter(formatter)
    ax.yaxis.grid(True, zorder=0)
    ax.yaxis.set_ticks_position('none')
    
    ax.set_ylim(np.array([males.min(), females.min()]).min()*.8, np.array([males.max(), females.max()]).max()*1.15)
    ax.set_xlim(years.min()-.5, years.max()+.5)
    
    # X axis
    ax.get_xaxis().set_visible(False)
    y_pct = (ax.get_ylim()[1] - ax.get_ylim()[0])  / 100

    # data value labels
    for i, year in enumerate(years):
        ax.text(year, ax.get_ylim()[0] - y_pct, year, va='top', ha='center')
        
    # border
    [spine.set_visible(False) for spine in ax.spines.values()]
        
	#     plt.show() # for debugging purposes
    fig_path = os.path.join('static', 'images', 'plots', 
                            state_template_values['state_stub'], 'driver_licenses.png')
    plt.savefig(fig_path, dpi=300, bbox_inches="tight", pad_inches=0)
    plt.close()

# state_name='Alaska'
# state_template_values = template_values(df, state_name)
# if True:
    
def highway_funding_revenue_chart(df, state_template_values):
    state_name = state_template_values['state']
    if state_name == 'Dist. Of Col.':
        state_name = 'Dist. of Col.'
    year = df['Year'].max()
    plt.clf()

    fig, ax = plt.subplots(figsize=(7.5, 1.1), dpi=96)
    plt.subplots_adjust(left=0, right=1)
    
    try:
        df1 = df.loc[
                (df['State'] == state_name) & 
                (df['Year'] == year) & 
                (df['Topic'] == 'Revenues Received by State for Highways')
            ].copy()

        items = ['Bonds','Federal Payments','General Fund Appropriations','Highway User Revenues',
                'Local Payments','Miscellaneous','Other State Imports']
        item_names = ['Bonds','Federal\nPayments','General Fund\nAppropriations','Highway User\nRevenues',
                'Local\nGovernments\nand Other','Miscellaneous','Other State\nImports']
        df1 = df1.set_index('Item')
        total = df1.loc['Total', 'Value']
    
    except Exception as err:
#        err.state = state
        err.state = state_name
        err.year = year
        err.custom = 'Highway Funding Chart'
        raise
        
    values = []
    for k in items:
        try:
            values.append(df1.loc[k, 'Value'])
        except KeyError:
            values.append(0)
    
    width_pct = total / 100
    min_width = width_pct * 17.5
    
    cumsum = 0
    ticks = []
    
    for i, value in enumerate(values):
        if value==0:
            value = width_pct / 4
        elif value < width_pct:
            value = width_pct
            
        if value < min_width:
            cumsum += .5 * (min_width - value)
        ax.barh(.8, value, .4, left=cumsum, align='center', color=CM[i], edgecolor=CM_DARK[i] )
        
        ticks.append(cumsum + .5*value)
        
        cumsum += value
        if value < min_width:
            cumsum += .5 * (min_width - value)
        
    ax.set_ylim(0, 1)

    ax.get_yaxis().set_visible(False)
    ax.get_xaxis().set_visible(False)
    
    # data value labels
    for i, value in enumerate(values):
        va = 'top'
        ax.text(ticks[i], .5, '${:,.0f}'.format(value), ha='center', va=va, fontsize=9, color=CM_DARK[i])
        ax.text(ticks[i], .3, item_names[i], ha='center', va=va, family='DejaVu Sans', fontsize=7, color=CM_DARK[i])

    # border
    [spine.set_visible(False) for spine in ax.spines.values()]

	#     plt.show() # for debugging purposes
    fig_path = os.path.join('static', 'images', 'plots', 
                            state_template_values['state_stub'], 'highway_funding_revenue.png')
    plt.savefig(fig_path, dpi=300, bbox_inches="tight", pad_inches=0)
    plt.close()

# state_name='Alabama'
# state_template_values = template_values(df, state_name)
# if True:
    
def highway_funding_expenditures_chart(df, state_template_values):
    state_name = state_template_values['state']
    if state_name == 'Dist. Of Col.':
        state_name = 'Dist. of Col.'
    year = df['Year'].max()
    plt.clf()

    fig, ax = plt.subplots(figsize=(7.5, 1.1), dpi=96)
    plt.subplots_adjust(left=0, right=1)
    
    try:
        df1 = df.loc[
                (df['State'] == state_name) & 
                (df['Year'] == year) & 
                (df['Topic'] == 'Disbursements Used by States for Highways')
            ].copy()

        items = ['Capital Outlay','Interest','Maintenance and Services','Administration and Misc',
                'Bond Retirement','Grant-In-Aid to Locals','Highway Law Enforcement & Safety']
        item_names = ['Capital Outlay','Interest','Maintenance\nand Services','Administration\nand Misc.',
                'Bond\nRetirement','Grants-in-aid\nto Locals','Highway Law\nEnforcment']

        df1 = df1.set_index('Item')
        total = df1.loc['Total', 'Value']
        
        
    except Exception as err:
#        err.state = state
        err.state = state_name
        err.year = year
        err.custom = 'Highway Funding Chart'
        raise
        
    values = []
    for k in items:
        try:
            values.append(df1.loc[k, 'Value'])
        except KeyError:
            values.append(0)
    
    width_pct = total / 100
    min_width = width_pct * 17.5
    
    cumsum = 0
    ticks = []
    
    for i, value in enumerate(values):
        if value==0:
            value = width_pct / 4
        elif value < width_pct:
            value = width_pct
            
        if value < min_width:
            cumsum += .5 * (min_width - value)
        ax.barh(.8, value, .4, left=cumsum, align='center', color=CM[i], edgecolor=CM_DARK[i] )
        
        ticks.append(cumsum + .5*value)
        
        cumsum += value
        if value < min_width:
            cumsum += .5 * (min_width - value)
        
    ax.set_ylim(0, 1)

    ax.get_yaxis().set_visible(False)
    ax.get_xaxis().set_visible(False)
    
    # data value labels
    for i, value in enumerate(values):
        va = 'top'
        ax.text(ticks[i], .5, '${:,.0f}'.format(value), ha='center', va=va, fontsize=9, color=CM_DARK[i])
        ax.text(ticks[i], .3, item_names[i], ha='center', va=va, family='DejaVu Sans', fontsize=7, color=CM_DARK[i])

    # border
    [spine.set_visible(False) for spine in ax.spines.values()]

	#     plt.show() # for debugging purposes
    fig_path = os.path.join('static', 'images', 'plots', 
                            state_template_values['state_stub'], 'highway_funding_expenditures.png')
    plt.savefig(fig_path, dpi=300, bbox_inches="tight", pad_inches=0)
    plt.close()

def url_for(base, filename=None):
    #return base + '/' + filename
    return filename

def render_html(templateVars):
    # Render HTML Tempalte
    # An environment provides the data necessary to read and  parse our templates. 
    templateEnv = Environment( 
        loader = FileSystemLoader( searchpath="templates" ),
        autoescape = select_autoescape(['html', 'xml']),
        auto_reload = True#,
        #extensions=[Test]
    )

    # Read the template file using the environment object.
    # This also constructs our Template object.
    template = templateEnv.get_template('ssa.html')

    outputText = template.render( templateVars, url_for = url_for )
    
    with open(os.path.join('static', templateVars['state_stub'] + '.html'), 'w') as f:
        f.write(outputText)

def write_pdf(state_stub, year):
    # write to PDF
    weasyprint.HTML('static/%s.html' % state_stub).write_pdf(
        'pdfs/%s_%s.pdf' % (state_stub, year)
    )



from workbook import Workbook

def read_abstracts(filename):
    lookup = pd.read_csv(os.path.join('data', 'excel_lookup_table.csv'))
    wb = Workbook(filename) # xlrd.open_workbook(filename)
    # for each sheet and each item in lookup table, iterate through and add to list
    facts = list()
    
    def mk_int(s):
        s = str(s).strip()
        try:
            return int(float(s)) if s else 0
        except ValueError:
            return 0
    
    def mk_float(s):
        s = str(s).strip()
        try:
            return float(s) if s else 0
        except ValueError:
            return 0
    
    for s in wb.sheet_names():
        if s not in ('AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 
                          'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 
                          'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 
                          'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 
                          'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 
                          'VT', 'VA', 'WA', 'WV', 'WI', 'WY'):
            continue
        wb.set_sheet(s)
        if wb.get_value(9, 1) == None:
            raise Exception("Error reading abstract file " +
                            filename + ". Did you remember to open, save, and close the generated " +
                            "abstracts Excel file? Please open file, save, close, and try again.")
            
        for idx, item in lookup.iterrows():
            try:
                fact = {
                    'State': str(wb.get_value(0,0)).title(), 
                    'Year': int(wb.get_value(0,1)), 
                    'Topic': item['Topic'], 
                    'Item': item['Item'], 
                    'Value': mk_float(wb.get_value(item['Row'], item['Value Column'])), 
                    'Distribution': mk_float(wb.get_value(item['Row'], item['Distribution'])), 
                    'Share of National': mk_float(wb.get_value(item['Row'], item['Share of National'])), 
                    'Unit': np.nan, 
                    'Highway Statistics Table': np.nan
                }
                facts.append(fact)
            except ValueError as err:
                err.custom = ' '.join(['Reading', item['Topic'], item['Item'], s])
                err.custom += '. \nPlease verify the row/column layout in ' + filename
                raise
                                    
            except IndexError as err:
                print('Error reading %s' % (s.name))
                print(item)
                err.custom = ' '.join(['Reading', item['Topic'], item['Item'], s, 'in', filename])
                raise
                
            except Exception as err:
                err.custom = 'Reading ' + filename
                raise
            
    return pd.DataFrame(facts)


def read_baseline_data():
    # Read in source dataset as a DataFrame
    types = {
        'State': 'str',
        'Year': 'int32',
        'Topic': 'str',
        'Item': 'str',
        'Value': 'float64',
        'Distribution': 'float64',
        'Share of National': 'float64',
        'Unit': 'str',
        'Highway Statistics Table': 'str'
    }
    df = pd.read_csv(os.path.join('data','data-clean-v8.csv'), dtype=types)  # 2010 - 2014

    try:
        df1 = read_abstracts('data/Stateabstract2015.xls')  # 2015
    except Exception:
        raise
    
    df = df.append(df1)
    
    try:
        df2 = read_abstracts('data/abstract_2016.xlsx')  # 2016
    except Exception:
        raise
        
    df = df.append(df2)

    return df

def create_zip(year):
    for file in os.listdir(path='pdfs'):
        if file[-4:]=='.pdf':
            shutil.copyfile(os.path.join('pdfs',file), os.path.join('static', 'downloads', file))
    with ZipFile(os.path.join('static', 'downloads','abstracts_'+ year + '.zip'), 'w') as myzip:
        for file in os.listdir(path=os.path.join('static','downloads')):
            if file[-4:]=='.pdf':
                myzip.write(os.path.join('static', 'downloads', file), arcname=file)
