FROM ubuntu:18.04

RUN apt-get update
RUN apt-get -y upgrade

RUN apt-get install -y build-essential python3-dev python3-pip python3-cffi libcairo2 libpango1.0-0 libgdk-pixbuf2.0-0 libffi-dev shared-mime-info
RUN apt-get install -y git

# update pip
RUN python3 -m pip install pip --upgrade
RUN python3 -m pip install wheel

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
ADD . /app

# Install any needed packages specified in requirements.txt
RUN pip3 install -r requirements.txt

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV NAME World
# ENV FLASK_DEBUG 1
ENV PYTHONDONTWRITEBYTECODE 1

ENV FLASK_APP app.py
# ENV FLASK_ENV development

ENV LC_ALL C.UTF-8
ENV LANG C.UTF-8

# Run app.py when the container launches
ENTRYPOINT ["python3"]
CMD ["app.py"]