#!/bin/bash
# Serve the SSA app
# to run: ./serve.sh

source activate ssa
export FLASK_APP=app.py
export FLASK_DEBUG=1
flask run