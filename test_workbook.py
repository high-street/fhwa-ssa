# testing
from workbook import Workbook

def test_sheet_names():
    for file in ['tests/abstract-template.xls', 'tests/abstract-template.xlsx']:    
        wb = Workbook(file)
        names = wb.sheet_names()
        assert 'AK' in names
    
def test_get_value():
    for file in ['tests/abstract-template.xls', 'tests/abstract-template.xlsx']:    
        wb = Workbook(file, sheet_index = 2)
        assert wb.get_value(0, 0) == 'ALABAMA'
    
def test_set_sheet():
    for file in ['tests/abstract-template.xls', 'tests/abstract-template.xlsx']:   
        wb = Workbook(file)
        wb.set_sheet('AK')
        assert wb.get_value(0, 0) == 'ALASKA'

def test_get_value_with_formula():
     for file in ['tests/abstract_2016.xls', 'tests/abstract_2017.xlsx']:   
        wb = Workbook(file)
        wb.set_sheet('AK')
        assert wb.get_value(7, 1) + wb.get_value(8, 1) == wb.get_value(9, 1) 