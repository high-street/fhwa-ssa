"""
FHWA State Statistical Abstracts Generator
Author: Mark Egge, High Street Consulting Group
Last Revision: 15 Dec 2018
"""

# to do
# - error handling for spreadsheets
# - remove directions sheet
# - input range validation

import os
import shutil
import logging

from flask import Flask, flash, render_template, request, redirect, url_for, send_from_directory
from werkzeug.utils import secure_filename
import time
# from threading import Thread
# from multiprocessing.pool import ThreadPool
import threading

import pandas as pd

import xlrd

from workbook import Workbook

import openpyxl
from openpyxl.drawing.image import Image
from openpyxl.styles import Border, Side, PatternFill, Font, GradientFill, Alignment

# finally, import my notebook with the charts methods:
# import nbfinder
import charts


# setup logging
LOG_PATH = os.path.join('static', 'app.log')
logging.basicConfig(filename=LOG_PATH,level=logging.DEBUG)
logging.basicConfig(format='%(asctime)s %(message)s')

# Initialize the Flask application
app = Flask(__name__)

# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = 'uploads/'

# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['xls', 'xlsx'])

app.secret_key = 'not so secret secret key'
app.config['SESSION_TYPE'] = 'filesystem'

STATUS_FILE = os.path.join('static', 'downloads', 'status.txt')
ERROR_FILE = os.path.join('static', 'downloads', 'error.txt')

state_abbr = {
    'Alabama': 'AL',    'Alaska': 'AK',    'Arizona': 'AZ',    'Arkansas': 'AR',
    'California': 'CA',    'Colorado': 'CO',    'Connecticut': 'CT',    'Delaware': 'DE',
    'D.C.': 'DC',    'Florida': 'FL',    'Georgia': 'GA',    'Hawaii': 'HI',
    'Idaho': 'ID',    'Illinois': 'IL',    'Indiana': 'IN',    'Iowa': 'IA',
    'Kansas': 'KS',    'Kentucky': 'KY',    'Louisiana': 'LA',    'Maine': 'ME',
    'Maryland': 'MD',    'Massachusetts': 'MA',    'Michigan': 'MI',    'Minnesota': 'MN',
    'Mississippi': 'MS',    'Missouri': 'MO',    'Montana': 'MT',    'Nebraska': 'NE',
    'Nevada': 'NV',    'New Hampshire': 'NH',    'New Jersey': 'NJ',    'New Mexico': 'NM',
    'New York': 'NY',    'North Carolina': 'NC',    'North Dakota': 'ND',    'Ohio': 'OH',
    'Oklahoma': 'OK',    'Oregon': 'OR',    'Pennsylvania': 'PA',    'Rhode Island': 'RI',
    'South Carolina': 'SC',    'South Dakota': 'SD',    'Tennessee': 'TN',    'Texas': 'TX',
    'Utah': 'UT',    'Vermont': 'VT',    'Virginia': 'VA',    'Washington': 'WA',
    'West Virginia': 'WV',    'Wisconsin': 'WI',    'Wyoming': 'WY',
}
state_name = dict(zip(state_abbr.values(), state_abbr.keys()))


# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

@app.before_first_request
def setup_webapp():
    print('Application Initialized. In @app.before_first_request')
    print('Application log: ' + LOG_PATH)
    logging.info('Application Initiated')

@app.route('/')
def index():
    for t in threading.enumerate():
        if t.getName() == 'pdf-builder':
            return redirect(url_for('process'))
    
    clear_temp_files()  # remove any files from previous runs
    
    return render_template('index.html')

@app.route("/upload", methods=["POST"])
def upload():
    uploaded_files = request.files.getlist("file[]")
    print(uploaded_files)
    files = list()
    for file in uploaded_files:
        if file and allowed_file(file.filename):
            file.save(os.path.join('uploads/', file.filename))
            files.append(os.path.join('uploads/', file.filename))
    
    logging.info('Uploaded files: ' + str(files))

    action = request.values.get("act")

    if action == "spreadsheet":
        build_abstract_spreadsheet(files, request.values.get("spreadsheet_year_select"))
        return redirect(url_for('download_spreadsheet'))

    if action == "pdf":
        with open(STATUS_FILE, 'w') as f:
            f.write('Initiated.')

        thr = threading.Thread(target=build_pdfs_async, name='pdf-builder', args=(files,))
        thr.start()

        return redirect(url_for('process'))
    
# poll the app to determine status
@app.route('/process')
def process():
    # going the poor man's route, here. Proper async implementation is with celery
    # instead, just going to poll for the presence of a results zip
    # or an error text file

    zip_files = [file for file in os.listdir(path=os.path.join('static', 'downloads')) if file[-4:]=='.zip']
    if len(zip_files) >= 1:
        return redirect(url_for('download', result='Successfully built all files.'))
    
    elif os.path.exists(ERROR_FILE):
        with open(ERROR_FILE, 'r') as f:
            error = f.read()
        flash('Error processing supplied abstracts file: ' + error)
        os.remove(ERROR_FILE)
        return redirect(url_for('index'))

    elif os.path.exists(STATUS_FILE):
        if 'pdf-builder' not in [t.getName() for t in threading.enumerate()]:
            flash('Processing unexpectedly terminated.')
            os.remove(STATUS_FILE)
            return redirect(url_for('index'))

        files = [file for file in os.listdir(path='pdfs') if file[-4:]=='.pdf']
        return render_template('processing.html', files = files)
    else:
        flash('No current process or output. Please try again.')
        return redirect(url_for('index'))

@app.route('/download')
def download():
    if os.path.exists(ERROR_FILE):
        with open(ERROR_FILE, 'r') as f:
            error = f.read()
        flash('Error processing input: ' + error)
        os.remove(ERROR_FILE)
        return redirect(url_for('index'))

    downloads_dir = os.path.join('static', 'downloads')
    pdfs = sorted([file for file in os.listdir(path=downloads_dir) if file[-4:]=='.pdf'])
    zip_file = [file for file in os.listdir(path=downloads_dir) if file[-4:]=='.zip']
    
    if len(zip_file) >= 1:
        zip_file = zip_file[0]
    else:
        return redirect(url_for('index'))

    return render_template('download.html', files=pdfs, zip_file=zip_file, result=request.args.get('result'))

@app.route('/download-spreadsheet')
def download_spreadsheet():
    downloads_dir = os.path.join('static', 'downloads')
    xlsx_file = [file for file in os.listdir(path=downloads_dir) if file[-4:]=='xlsx']
    
    if len(xlsx_file) >= 1:
        xlsx_file = xlsx_file[0]
    else:
        return redirect(url_for('index'))

    return render_template('download-spreadsheet.html', xlsx_file=xlsx_file)

# Some testing / utility functions
@app.route('/clean-up')
def clear_temp_files():
    clean_up()
    set_up()
    return "Cleaned Up Previous Runs"


@app.route('/cancel')
def kill():
    threads = []
    for t in threading.enumerate():
        #threads.append(t.getName())
        if t.getName() == 'pdf-builder':
            t.quit = True
            t.join()

    return redirect(url_for('download'))
    # return "sent kill signal to " + '|'.join([t for t in threads])


# processing functions

def set_up():
    # some setup - ensure require diretories exist
    directories = ['uploads', 'pdfs', 'static/images', 'static/images/plots', 'static/downloads']
    for directory in directories:
        if not os.path.exists(directory):
            os.makedirs(directory)


# delete output from previous runs
def clean_up(): 
    # clean up previous runs
    path='static'
    for file in os.listdir(path=path):
        if file[-5:]=='.html':
            os.remove(os.path.join(path, file))

    path='pdfs'
    for file in os.listdir(path=path):
        if file[-4:]=='.pdf':
            os.remove(os.path.join(path, file))

    import shutil
    directories = ['uploads', 'pdfs', 'static/images/plots', 'static/downloads']
    for directory in directories:
        if os.path.exists(directory):
            shutil.rmtree(directory)


def build_abstract_spreadsheet(files, year):
    logging.info('Spreadsheet building initiated')
    logging.info(files)
    
    year = int(year)

    def style_range(ws, cell_range, border=Border(), fill=None, font=None, alignment=None):
        """
        Apply styles to a range of cells as if they were a single cell.
        """

        top = Border(top=border.top)
        left = Border(left=border.left)
        right = Border(right=border.right)
        bottom = Border(bottom=border.bottom)

        first_cell = ws[cell_range.split(":")[0]]
        if alignment:
            ws.merge_cells(cell_range)
            first_cell.alignment = alignment

        rows = ws[cell_range]
        if font:
            first_cell.font = font

        for cell in rows[0]:
            cell.border = cell.border + top
        for cell in rows[-1]:
            cell.border = cell.border + bottom

        for row in rows:
            l = row[0]
            r = row[-1]
            l.border = l.border + left
            r.border = r.border + right
            if fill:
                for c in row:
                    c.fill = fill

    # going to assume that the files uploaded are in the correct order
    template = openpyxl.load_workbook(filename = 'static/state_abstracts_template.xlsx')

    # replace images removed by openpyxl, set year
    dest = template['National']
    img = Image('static/images/flags/national.png')
    
    img.width = 84
    img.height = 56
    dest.add_image(img, 'C1')
    dest['B1'] = year

    for state in state_abbr.values():
        dest = template[state] # load sheet

        img = Image('static/images/flags/' + 
                    state_name[state].lower().replace(' ', '_') + '.png')
        
        img.width = 84
        img.height = 56

        dest.add_image(img, 'H1')
        
        dest['B1'] = year


    # population- DL-1c
    dl1c = Workbook(files[0])
    source_row = 14
    for state in state_abbr.values():
        dest = template[state] # load sheet
        dest['B4'] = int(dl1c.get_value(source_row, 7))
        source_row = source_row + 1


    # total land area - PS-1
    source = Workbook(files[1])
    source_row = 14
    for state in state_abbr.values():
        dest = template[state] # load sheet
        dest['B5'] = round(source.get_value(source_row, 1) + source.get_value(source_row, 2), 0)
        source_row = source_row + 1


    # fuels - MF33GA and MF-33SF
    ga = Workbook(files[2])
    sf = Workbook(files[3])
    source_row = 13
    for state in state_abbr.values():
        dest = template[state] # load sheet
        dest['B8'] = ga.get_value(source_row, 13) 
        dest['B9'] = sf.get_value(source_row, 13) 
        source_row = source_row + 1

    # gasoline - MF21
    source = Workbook(files[4])
    source_row = 8
    for state in state_abbr.values():
        dest = template[state] # load sheet
        dest['B11'] = source.get_value(source_row, 5) 
        dest['B12'] = source.get_value(source_row, 8) 
        source_row = source_row + 1

    # Road Length - HM20
    source = Workbook(files[5], sheet_name = 'A') 
    # Fun fact, there's a hidden sheet called "CRYSTAL_PERSIST"
    source_row = 14
    for state in state_abbr.values():
        dest = template[state] # load sheet
        dest['B15'] = source.get_value(source_row, 8)
        dest['B16'] = source.get_value(source_row, 16)
        dest['B20'] = source.get_value(source_row, 1) + source.get_value(source_row, 9)
        dest['B21'] = source.get_value(source_row, 2) + source.get_value(source_row, 10)
        dest['B22'] = source.get_value(source_row, 3) + source.get_value(source_row, 11)
        dest['B23'] = source.get_value(source_row, 4) + source.get_value(source_row, 12)
        dest['B24'] = source.get_value(source_row, 5) + source.get_value(source_row, 13)
        dest['B25'] = source.get_value(source_row, 6) + source.get_value(source_row, 14)
        dest['B26'] = source.get_value(source_row, 7) + source.get_value(source_row, 15)
        source_row = source_row + 1


    # Functional System Lane Length - HM60
    source = Workbook(files[6], sheet_name = 'A') 
    source_row = 14
    for state in state_abbr.values():
        dest = template[state] # load sheet
        dest['B37'] = source.get_value(source_row, 1) + source.get_value(source_row, 9)
        dest['B38'] = source.get_value(source_row, 2) + source.get_value(source_row, 10)
        dest['B39'] = source.get_value(source_row, 3) + source.get_value(source_row, 11)
        dest['B40'] = source.get_value(source_row, 4) + source.get_value(source_row, 12)
        dest['B41'] = source.get_value(source_row, 5) + source.get_value(source_row, 13)
        dest['B42'] = source.get_value(source_row, 6) + source.get_value(source_row, 14)
        dest['B43'] = source.get_value(source_row, 7) + source.get_value(source_row, 15)
        source_row = source_row + 1

    # VMT - VM-2
    source = Workbook(files[7], sheet_name = 'A') 
    source_row = 14
    for state in state_abbr.values():
        dest = template[state] # load sheet
        dest['B46'] = source.get_value(source_row, 8)
        dest['B47'] = source.get_value(source_row, 16)
        dest['B51'] = source.get_value(source_row, 1) + source.get_value(source_row, 9)
        dest['B52'] = source.get_value(source_row, 2) + source.get_value(source_row, 10)
        dest['B53'] = source.get_value(source_row, 3) + source.get_value(source_row, 11)
        dest['B54'] = source.get_value(source_row, 4) + source.get_value(source_row, 12)
        dest['B55'] = source.get_value(source_row, 5) + source.get_value(source_row, 13)
        dest['B56'] = source.get_value(source_row, 6) + source.get_value(source_row, 14)
        dest['B57'] = source.get_value(source_row, 7) + source.get_value(source_row, 15)
        source_row = source_row + 1

    # Driver Licenses - DL-22
    males = Workbook(files[8], sheet_name = 'MALES') 
    females = Workbook(files[8], sheet_name = 'FEMALES') 
    source_row = 14
    for state in state_abbr.values():
        dest = template[state] # load sheet
        dest['G4'] = males.get_value(source_row, 16)
        dest['G5'] = females.get_value(source_row, 16)
        source_row = source_row + 1


    # Motor Fuel Tax Rates - MF-121T
    source = Workbook(files[9], sheet_name = 'MF121TP1') 

    source_row = 11
    for state in state_abbr.values():
        dest = template[state] # load sheet
        dest['G8'] = source.get_value(source_row, 2)
        dest['G9'] = source.get_value(source_row, 4)
        dest['G10'] = source.get_value(source_row, 8)
        source_row = source_row + 1

    # Vehicle Registrations - MV-1
    source = Workbook(files[10]) 
    source_row = 12
    for state in state_abbr.values():
        dest = template[state] # load sheet
        dest['G13'] = source.get_value(source_row, 3)
        dest['G14'] = source.get_value(source_row, 6)
        dest['G15'] = source.get_value(source_row, 9)
        dest['G17'] = source.get_value(source_row, 12)
        source_row = source_row + 1

    # Fatal Injuries - FI-20
    source = Workbook(files[11]) 
    source_row = 14
    for state in state_abbr.values():
        dest = template[state] # load sheet
        dest['G20'] = source.get_value(source_row, 8)
        dest['G21'] = source.get_value(source_row, 16)
        dest['G22'] = source.get_value(source_row, 17)
        source_row = source_row + 1

    # Revenues - SF-1
    source = Workbook(files[12]) 
    source_row = 14
    for state in state_abbr.values():
        dest = template[state] # load sheet
        dest['G31'] = source.get_value(source_row, 7)
        dest['G32'] = source.get_value(source_row, 8)
        dest['G33'] = source.get_value(source_row, 9)
        dest['G34'] = source.get_value(source_row, 10)
        dest['G35'] = source.get_value(source_row, 11) + source.get_value(source_row, 12)
        dest['G36'] = source.get_value(source_row, 13) + source.get_value(source_row, 14)
        dest['G37'] = source.get_value(source_row, 15)
        source_row = source_row + 1

    # Disbersements - SF-2
    source = Workbook(files[13]) 
    source_row = 14
    for state in state_abbr.values():
        dest = template[state] # load sheet
        dest['G41'] = source.get_value(source_row, 3)
        dest['G42'] = source.get_value(source_row, 6)
        dest['G43'] = source.get_value(source_row, 7)
        dest['G44'] = source.get_value(source_row, 8)
        dest['G45'] = source.get_value(source_row, 9)
        dest['G46'] = source.get_value(source_row, 10) + source.get_value(source_row, 11)
        dest['G47'] = source.get_value(source_row, 12)
        source_row = source_row + 1
        

    # Attribuetd Fed Trust Fund Receipts - FE-9
    hwy = Workbook(files[14], sheet_index = 0) 
    transit = Workbook(files[14], sheet_index = 1) 
    # careful, here—we want the first sheet ("FE-9 pg 1") but I'm afraid it won't
    # be named consistently from one year to the next
    source_row = 14
    for state in state_abbr.values():
        dest = template[state] # load sheet
        dest['G50'] = hwy.get_value(source_row, 7)
        dest['G51'] = transit.get_value(source_row, 4)
        source_row = source_row + 1


    # Style Cells
    lights = ['FFA6CEE3', 'FFB2DF8A', 'FFFDBF6F', 'FFCAB2D6', 'FFFFFF99']
    darks =  ['FF1F78B4', 'FF33A02C', 'FFFF7F00', 'FF6A3D9A', 'FFB15928']

    light = openpyxl.styles.colors.Color(rgb = lights[year % 5])
    dark = openpyxl.styles.colors.Color(rgb = darks[year % 5])

    bold_font = Font(bold=True, italic=True, color='FFFFFFFF')

    light_fill = openpyxl.styles.fills.PatternFill(patternType='solid', fgColor = light)
    dark_fill = openpyxl.styles.fills.PatternFill(patternType='solid', fgColor = dark)


    tabs = ['National'] + list(state_abbr.values())

    for sheet_name in ['National'] + list(state_abbr.values()):
        ws = template[sheet_name]
        c = ws['A1']
        c.font = Font(name = 'Arial Black', size = 16, color = 'FFFFFFFF')
        c = ws['b1']
        c.font = Font(name = 'Arial Black', size = 16, color = 'FFFFFFFF')
        
    for sheet_name in state_abbr.values():
        ws = template[sheet_name]
        style_range(ws, 'A1:I2', fill = dark_fill)

    for sheet_name in state_abbr.values():
        for rng in ['A4:A5', 'A8:A9', 'A11:A12', 'A15:A16', 'A20:A26', 'A37:A43',
                   'A46:A47', 'A51:A57', 'F4:F5', 'F8:F10', 'F13:F15', 'F17:F17',
                   'F20:F22', 'F31:F37', 'F41:F47', 'F50:F51', 'C4:C5']:
            style_range(template[sheet_name], rng, fill = light_fill)

    for sheet_name in state_abbr.values():
        for rng in ['A10:D10', 'A17:D17', 'A27:D27', 'A44:D44', 'A48:D48', 'A58:D58',
                    'F6:I6', 'F16:I16', 'F18:I18', 'F23:I23', 'F38:I38',
                    'F48:I48', 'F52:I52']:
            style_range(template[sheet_name], rng, font = bold_font, fill = dark_fill)

    for sheet_name in state_abbr.values():
        ws = template[sheet_name]
        for cell_name in ['B10', 'C10', 'B17', 'C17', 'D17', 'B27', 'C27', 'D27', 'B44', 'C44', 'D44',
                          'B48', 'C48', 'D48', 'B58', 'C58', 'D58', 'G6', 'H6', 'I6', 'G16',
                          'H18', 'H18', 'I18', 'G23', 'H23', 'I23', 'G38', 'H38', 'I38',
                          'G48', 'H48', 'I48', 'G52', 'H52', 'I52']:
            c = ws[cell_name]
            c.font = Font(italic = True, color = 'FFFFFFFF')

    # NATIONAL
    ws = template['National']
    style_range(ws, 'A1:H2', fill = dark_fill)
    for rng in ['A4:A5', 'A8:A9', 'A11:A12', 'A15:A16', 'A20:A26', 'A37:A43',
                'A48:A49', 'A53:A59', 
                'F4:F5', 'F8:F10', 'F13:F15', 'F17:F17',
                'F20:F22', 'F31:F37', 'F42:F48', 'F52:F53']:
        style_range(ws, rng, fill = light_fill)
        
    for rng in ['A10:C10', 'A17:C17', 'A27:C27', 'A44:C44', 'A50:C50', 'A60:C60',
                'F6:H6', 'F16:H16', 'F18:H18', 'F23:H23', 'F38:H38',
                'F49:H49', 'F54:H54']:
        style_range(ws, rng, font = bold_font, fill = dark_fill)
        
    for cell_name in ['B10', 'C10',  'B17', 'C17', 
                      'B27', 'C27', 'B44', 'C44', 
                      'B50', 'C50', 
                      'B60', 'C60', 'F16', 'G16',
                      'F18', 'G18', 'H18',
                      'F23', 'G23', 'H23',
                      'F38', 'G38', 'H38',
                      'G6', 'H6', 'G16',
                      'H18', 'H18', 
                      'G23', 'H23',
                      'G38', 'H38',
                      'G49', 'H49', 
                      'G54', 'H54']:
        c = ws[cell_name]
        c.font = Font(italic = True, color = 'FFFFFFFF')

    template.save('static/downloads/abstract_' + str(year) + '.xlsx')



def build_pdfs_async(files):
    logging.info('Async PDF Build Initiated')

    df = charts.read_baseline_data()
    try:
        for file in files: # ['data/state_abstracts_2016.xls', 'data/state_abstracts_2017.xls']:        
            df1 = charts.read_abstracts(file)
            logging.info('Read %s with %s records for year %s' % ( file, str(len(df1)), df1['Year'].min() ) )
            df = df.append(df1)

        year = str(df['Year'].max())
        
        logging.info('Preparing to build PDFs for %s. Total records: %s' % (year, str(len(df))))

        states = pd.unique(df['State'].values)
        if len(states) != 51:
            print('Wrong number of states. Expecting 51. Got %s.' % str(len(states)))
            raise ValueError('Invalid number of states.')

        t = threading.currentThread()

        for state_name in states:
            if getattr(t, "quit", False):
                logging.info("Received kill signal. Exiting gracefully.")
                break

            logging.info('Processing ' + state_name)
            state_template_values = charts.template_values(df, state_name)
            directory = 'static/images/plots/%s' % state_template_values['state_stub']
            if not os.path.exists(directory):
                os.makedirs(directory)
            charts.p1_pct_ntnl_plot(state_template_values)
            charts.p1_line_charts(df, state_template_values)
            charts.road_length_chart(df, state_template_values)
            charts.share_of_road_miles_chart(df, state_template_values)
            charts.road_length_vmt_rural_urban_chart(df, state_template_values)
            
            charts.vehicle_miles_traveled_chart(df, state_template_values)
            charts.new_lane_miles_chart(df, state_template_values)
            
            charts.vehicle_registrations_chart(df, state_template_values)
            charts.vehicle_crash_fatalities_chart(df, state_template_values)
            charts.driver_licenses_chart(df, state_template_values)
            charts.highway_funding_revenue_chart(df, state_template_values)
            charts.highway_funding_expenditures_chart(df, state_template_values)
                
            charts.render_html(state_template_values)
            charts.write_pdf(state_template_values['state_stub'], year)
            
        logging.info('PDFs created. Building ZIP')
        charts.create_zip(year)

    except Exception as e:
        msg = str(e)
        if 'state' in dir(e):
            msg += ' State: ' + e.state
        if 'year' in dir(e):
            msg += ' Year: ' + str(e.year)
        if 'custom' in dir(e):
            msg += ' Details: ' + e.custom
        
        logging.warning(msg)
        with open(ERROR_FILE, 'w') as f:    
            f.write(msg)
        raise

    finally:
        if os.path.exists(STATUS_FILE):
            os.remove(STATUS_FILE)

    # finally:
    #     logging.info('Finished PDF build')

if __name__ == '__main__':
    print('SSA Generator Initiated. Please access through your web broswer.')
    logging.info('Setup complete')    

    app.run(
        host="0.0.0.0",
        port=int("80"),
        debug=True
    )